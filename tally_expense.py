import logging

from flask import request, jsonify;

from codeitsuisse import app;

logger = logging.getLogger(__name__)

@app.route('/tally-expense', methods=['POST'])




class expense_report:
    name = ""

    def __init__(self, name, paidamt, returnamt):
        self.name = name
        self.paidamt = paidamt
        self.returnamt = returnamt

def evaluate():

    data = request.get_json();

    #variable declaration
    report_name = data.get("name")
    persons_list = data.get("persons")
    expenses_list = data.get("expenses")



    expense_report.name = report_name
    #this will be a list of expense report objects
    listofexpenses = list()
    for i in range(len(persons_list)):
        person = expense_report(persons_list[i],0,0)
        listofexpenses.append(person)
    for expense in expenses_list:
        exclude_list = []
        cost = expense["amount"]
        #exclude cost
        if "exclude" in expense:
            exclude_list = expense["exclude"]
        each_cost = cost/(len(persons_list)-len(exclude_list))

        #distribute cost to everyone
        for i in listofexpenses:
            if i.name in exclude_list:
                continue
            temp = i.paidamt
            temp += each_cost
            i.paidamt = temp

    for expense in expenses_list:
        paid = expense["paidBy"]
        amt = expense["amount"]
        for i in listofexpenses:
            if paid == i.name:
                temp = i.returnamt
                temp += amt
                i.returnamt = temp
            else:
                pass
    #determine the amt owed to each person
    for i in listofexpenses:
        i.returnamt = i.paidamt - i.returnamt
    transactions = list()

    for i in listofexpenses:
        payer = i.name
        #retraverse the list to find nearest receiver
        for a in listofexpenses:
            if a.name == payer:
                pass
            if a.returnamt < 0:
                final = a.returnamt + i.returnamt

                if final <= 0:
                    payover = i.returnamt
                    i.returnamt = 0
                    a.returnamt =  final

                    trans = {
                        "from" : payer,
                        "to" : a.name,
                        "amount" : payover

                    }
                    transactions.append(trans)
                elif final > 0:
                    payover = a.returnamt
                    i.returnamt = final
                    a.returnamt = 0
                    trans = {
                        "from": payer,
                        "to": a.name,
                        "amount": -1 * payover

                    }
                    transactions.append(trans)



    logging.info("data sent for evaluation {}".format(data))
    logging.info("{}".format(transactions))
    return jsonify(transactions);




evaluate()
